import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UnitTests {
    @Test
    fun addition_isCorrect() {
        val inputString = "1,2,5"
        assertEquals(8, StringCalculator.addDefaultDelimiter(inputString))
    }

    @Test
    fun additionWithNewLines_isCorrect() {
        val inputString1 = "1\n,2,3"
        val inputString2 = "1,\n2,4"

        assertEquals(6, StringCalculator.addDefaultDelimiter(inputString1))
        assertEquals(7, StringCalculator.addDefaultDelimiter(inputString2))
    }

    @Test
    fun additionWithCustomDelimiter_isCorrect() {
        val inputString1 = "//;\n1;3;4"
        val inputString2 = "//$\n1$2$3"
        val inputString3 = "//@\n2@3@8"

        assertEquals(8, StringCalculator.add(inputString1))
        assertEquals(6, StringCalculator.add(inputString2))
        assertEquals(13, StringCalculator.add(inputString3))
    }

    @Test
    fun additionWithCustomDelimiterAndNegativeNumbers_isIncorrect() {
        val inputString = "//;\n1;-3;4"

        assertFailsWith<NegativeNotAllowedException> {
            StringCalculator.add(inputString)
        }
    }

    @Test
    fun additionWithCustomDelimiterIgnoresLargeNumbers_isCorrect() {
        val inputString = "2,1001"

        assertEquals(2, StringCalculator.addDefaultDelimiter(inputString))
    }

    @Test
    fun additionWithCustomArbitraryLengthDelimiter_isCorrect() {
        val inputString = "//***\n1***2***3"

        assertEquals(6, StringCalculator.add(inputString))
    }

    @Test
    fun additionWithCustomMultipleDelimiters_isCorrect() {
        val inputString = "//$,@\n1$2@3"

        assertEquals(6, StringCalculator.add(inputString))
    }

    @Test
    fun additionWithCustomArbitraryLengthMultipleDelimiters_isCorrect() {
        val inputString = "//$$$,@@@\n1$$$2@@@3"

        assertEquals(6, StringCalculator.add(inputString))
    }
}