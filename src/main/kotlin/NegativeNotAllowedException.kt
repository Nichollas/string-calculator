class NegativeNotAllowedException(override val message: String): Exception() {
    constructor(numbers: List<Int?>) : this("Negatives not allowed: $numbers")
}