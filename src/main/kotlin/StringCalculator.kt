import java.util.*

class StringCalculator {
    companion object {
        fun addDefaultDelimiter(value: String?): Int {
            return sumFromString(value, arrayOf(","))
        }

        // Accept Strings on the following format: //[delimiter]\n[delimiter separated numbers]
        fun add(value: String?): Int {
            if (value.isNullOrEmpty()) return 0

            val lines = value.split("\n", "\\n")
            if (lines.size < 2) return 0
            val delimiters = lines[0].replace("//","").split(",")

            return sumFromString(lines[1], delimiters.toTypedArray())
        }

        private fun sumFromString(value: String?, delimiters: Array<String>): Int {
            if (value.isNullOrEmpty()) return 0
            val stringNumbers = value.replace("\\n", "").replace("\n", "")
            val allNumbers = stringNumbers.split(*delimiters, ignoreCase = true).mapNotNull {
                it.toIntOrNull()
            }

            // Ignore wrong formatted and large numbers
            val numbers: List<Int> = allNumbers.filter { it <= 1000 }

            // Check if any number is negative
            val negatives = numbers.filter { it < 0 }
            if (negatives.isNotEmpty()) {
                throw NegativeNotAllowedException(negatives)
            }

            return numbers.sumOf { it }
        }
    }
}