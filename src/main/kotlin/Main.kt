fun main(args: Array<String>) {
    firstOption()
}

private fun firstOption() {
    println("We will test sum on String. You want to:" +
            "\n" +
            "1: Sum numbers separated by comma." +
            "\n" +
            "2: Sum numbers with custom delimiters.")

    selectOptions(readLine())
}

private fun selectOptions(option: String?) {
    when(option) {
        "1" -> {
            println("Enter numbers to be add separated by comma:")
            val inputString = readLine()
            val result = StringCalculator.addDefaultDelimiter(inputString)
            println("The result is: $result")
        }
        "2" -> {
            println("\n" +
                    "Enter String on the following format: //[delimiter]\\n[delimiter separated numbers]:")
            val inputString = readLine()
            val result = StringCalculator.add(inputString)
            println("The result is: $result")
        }

        else -> {
            println("Invalid option.")
        }
    }

    finishOrRepeat(option)
}

private fun finishOrRepeat(lastOption: String?) {
    println("Press" +
            "\n" +
            "1: Change Sum option." +
            "\n" +
            "2: Repeat last option." +
            "\n" +
            "Anything else: Finish.")

    when(readLine()) {
        "1" -> {
            firstOption()
        }
        "2" -> {
            selectOptions(lastOption)
        }

        else -> {
            return
        }
    }
}